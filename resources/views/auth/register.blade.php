@extends('layouts.auth')
@section('style')
<link href="{{asset('inspinia/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">
<div>
    <h1 class="logo-name">IN+</h1>
</div>
<h3>Register to Course AI</h3>
<p>Create account to see it in action.</p>
<form class="m-t" role="form" method="POST" action="{{ route('register') }}">
    @csrf
    <div class="form-group">
        <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
            placeholder="Name" required="">
        @if ($errors->has('name'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group">
        <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
            placeholder="Email" required="">
        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group">
        <input type="text" name="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}"
            placeholder="Username" required="">
        @if ($errors->has('username'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('username') }}</strong>
        </span>
        @endif
    </div>

    <div class="form-group">
        <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
            placeholder="Password" required="">
        @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
    <div class="form-group">
        <div class="checkbox i-checks"><label> <input type="checkbox"><i></i> Agree the terms and policy </label></div>
    </div>
    <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

    <p class="text-muted text-center"><small>Already have an account?</small></p>
    <a class="btn btn-sm btn-white btn-block" href="{{route('login')}}">Login</a>
</form>
<p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
</div>
@endsection

@section('script')
<script src="{{asset('inspinia/js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>
@endsection

@extends('layouts.auth')
@section('title')
<title>เข้าสู่ระบบ</title>
@endsection

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">AI</h1>
        </div>
        <h3>ยินดีต้อนรับสู่ Course AI</h3>
        <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
                @csrf
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username" name='username' required>
                @if ($errors->has('username'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password" name='password' required>
                @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">เข้าสู่ระบบ</button>

            <a href="{{ route('password.request') }}"><small>ลืมรหัสผ่าน?</small></a>
            <p class="text-muted text-center"><small>คุณยังไม่มีบัญชีใช่ไหม?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="{{route('register')}}">สร้างบัญชีใหม่</a>
        </form>
    </div>
</div>
@endsection
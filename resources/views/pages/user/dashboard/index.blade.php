@extends('layouts.user.main')

@section('style')

@endsection

@section('content')
@foreach ($courses as $course)
    <div class="row">
        @foreach ($course as $item)
            <a href="{{route('user.course.show',$item->id)}}">
                <div class="col-md-3">
                    <div class="ibox">
                        <div class="ibox-content product-box">
                            <div class="product-imitation">
                                <img src="{{$item->getUrlPath()}}" alt="" width="240" height="135">
                            </div>
                            <div class="product-desc">
                                <span class="product-price">
                                    ${{$item->price}}
                                </span>
                                <a href="{{route('user.course.show',2)}}" class="product-name"> {{$item->name}}</a>

                                <div class="small m-t-xs">
                                    {{$item->detail}}
                                </div>
                                <div class="m-t text-righ">
                                    <a href="{{route('user.course.show',2)}}" class="btn btn-xs btn-outline btn-primary">Info <i class="fa fa-long-arrow-right"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        @endforeach
    </div>
@endforeach
@endsection

@section('script')

@endsection
@extends('layouts.user.main')

@section('style')
    
@endsection

@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight article">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="text-center article-title">
                        <span class="text-muted"><i class="fa fa-clock-o"></i> 28th Oct 2015</span>
                        <h1>
                            Supervised Learning: Error-correction Learning Algorithm
                        </h1>
                    </div>
                    <p>
                        AI ถูกจำแนกเป็น 3 ระดับตามความสามารถหรือความฉลาดดังนี้
1 ) ปัญญาประดิษฐ์เชิงแคบ (Narrow AI ) หรือ ปัญญาประดิษฐ์แบบอ่อน (Weak AI) : คือ  AI ที่มีความสามารถเฉพาะทางได้ดีกว่ามนุษย์(เป็นที่มาของคำว่า Narrow(แคบ) ก็คือ AI ที่เก่งในเรื่องเเคบๆหรือเรื่องเฉพาะทางนั่นเอง)  อาทิ เช่น AI ที่ช่วยในการผ่าตัด(AI-assisted robotic surgery)  ที่อาจจะเชี่ยวชาญเรื่องการผ่าตัดกว่าคุณหมอยุคปัจจุบัน  แต่แน่นอนว่า AIตัวนี้ไม่สามารถที่จะทำอาหาร ร้องเพลง หรือทำสิ่งอื่นที่นอกเหนือจากการผ่าตัดได้นั่นเอง  ซึ่งผลงานวิจัยด้าน AI ณ ปัจจุบัน ยังอยู่ที่ระดับนี้
2 ) ปัญญาประดิษฐ์ทั่วไป (General AI )  : คือ AI ที่มีความสามารถระดับเดียวกับมนุษย์ สามารถทำทุกๆอย่างที่มนุษย์ทำได้และได้ประสิทธิภาพที่ใกล้เคียงกับมนุษย์
3) ปัญญาประดิษฐ์แบบเข้ม (Strong AI ) : คือ AI ที่มีความสามารถเหนือมนุษย์ในหลายๆด้าน
                    </p>
                    <p>
                        ปัจจุบัน ได้มีการนำ AI มาใช้ในอุตสาหกรรมจำนวนมาก  โดย "แมคคินซีย์แอนด์คอมปะนี (McKinsey & Company) "  (บริษัทที่ปรึกษาด้านการบริหารชั้นนำของโลก  )  ได้กล่าวไว้ว่า " AI มีศักยภาพในการทำเงินได้ถึง 600 ล้านดอลล่าร์สหรัฐในการขายปลีก  สร้างรายได้มากขึ้น 50 เปอร์เซนต์ในการธนาคารเมื่อเทียบกับการใช้เทคนิควิเคราะห์เเบบอื่นๆ  และสร้างรายได้มากกว่า 89 เปอร์เซนต์ ในการขนส่งและคมนาคม "
                    </p>
                    <p>
                        <i>
                            ยิ่งไปกว่านั้น หากฝ่ายการตลาดขององค์กรต่างๆ หันมาใช่ AI จะเป็นการเพิ่มศักยภาพให้กับการทำงานด้านการตลาดอย่างมาก เพราะว่า AI สามารถที่จะทำงานที่ซ้ำซากได้อย่างอัตโนมัติ ส่งผลให้ตัวแทนจำหน่าย สามารถที่จะโฟกัสไปที่การสนทนากับลูกค้า อาทิเช่น บริษัทนามว่า " Gong " มีบริการที่เรียกว่า "conversation intelligence"  , โดยทุกๆครั้งที่ตัวแทนจำหน่ายต่อสายคุยโทรศัพท์กับลูกค้า AIจะทำหน้าที่ในการบันทึกเสียงเเละวิเคราะห์ลูกค้าในขณะเดียวกัน มันสามารถแนะนำได้ว่าลูกค้าต้องการอะไร ควรจะคุยเเบบไหน ถือเป็นการซื้อใจลูกค้าอย่างหนึ่ง
                        </i>
                    </p>
                    <p>
                        โดยสรุป , ปัญญาประดิษฐ์หรือ AI เป็นเทคโนโลยีที่ล้ำสมัยที่สามารถรับมือกับปัญหาที่ซับซ้อนเกินกว่าที่มนุษย์จะสามารถรับมือได้  เเละ AI ยังเป็นเครื่องมือที่สามารถทำงานที่ซ้ำซากน่าเบื่อแทนมนุษย์ได้อย่างดีเยี่ยม ช่วยให้เราสามารถมีเวลาไปโฟกัสงานที่สำคัญและสามารถสร้างมูลค่าได้มากกว่า นอกจากนี้การประยุกต์ใช้ AI ในระดับอุตสาหกรรม ยังช่วยลดต้นทุนเเละเพิ่มรายได้มหาศาล
                    </p>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            
                        </div>
                        <div class="col-md-6">
                            <div class="small text-right">
                                <h5>Stats:</h5>
                                <div> <i class="fa fa-comments-o"> </i> 56 comments </div>
                                <i class="fa fa-eye"> </i> 144 views
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">

                            <h2>Comments:</h2>
                            <div class="social-feed-box">
                                <div class="social-avatar">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a1.jpg">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Andrew Williams
                                        </a>
                                        <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                                    </div>
                                </div>
                                <div class="social-body">
                                    <p>
                                        Many desktop publishing packages and web page editors now use Lorem Ipsum as
                                        their
                                        default model text, and a search for 'lorem ipsum' will uncover many web sites
                                        still
                                        default model text.
                                    </p>
                                </div>
                            </div>
                            <div class="social-feed-box">
                                <div class="social-avatar">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a2.jpg">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Michael Novek
                                        </a>
                                        <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                                    </div>
                                </div>
                                <div class="social-body">
                                    <p>
                                        Many desktop publishing packages and web page editors now use Lorem Ipsum as
                                        their
                                        default model text, and a search for 'lorem ipsum' will uncover many web sites
                                        still
                                        default model text, and a search for 'lorem ipsum' will uncover many web sites
                                        still
                                        in their infancy. Packages and web page editors now use Lorem Ipsum as their
                                        default model text.
                                    </p>
                                </div>
                            </div>
                            <div class="social-feed-box">
                                <div class="social-avatar">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a3.jpg">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Alice Mediater
                                        </a>
                                        <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                                    </div>
                                </div>
                                <div class="social-body">
                                    <p>
                                        Many desktop publishing packages and web page editors now use Lorem Ipsum as
                                        their
                                        default model text, and a search for 'lorem ipsum' will uncover many web sites
                                        still
                                        in their infancy. Packages and web page editors now use Lorem Ipsum as their
                                        default model text.
                                    </p>
                                </div>
                            </div>
                            <div class="social-feed-box">
                                <div class="social-avatar">
                                    <a href="" class="pull-left">
                                        <img alt="image" src="img/a5.jpg">
                                    </a>
                                    <div class="media-body">
                                        <a href="#">
                                            Monica Flex
                                        </a>
                                        <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                                    </div>
                                </div>
                                <div class="social-body">
                                    <p>
                                        Many desktop publishing packages and web page editors now use Lorem Ipsum as
                                        their
                                        default model text, and a search for 'lorem ipsum' will uncover many web sites
                                        still
                                        in their infancy. Packages and web page editors now use Lorem Ipsum as their
                                        default model text.
                                    </p>
                                </div>
                            </div>


                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('script')

@endsection


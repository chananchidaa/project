@extends('layouts.user.main')

@section('style')
    
@endsection

@section('content')
    <div class="row">
    <a href="{{route('user.course.show',2)}}">
    <div class="col-md-3">
        <div class="ibox">
            <div class="ibox-content product-box">

                <div class="product-imitation">
                    [ INFO ]
                </div>
                <div class="product-desc">
                    <span class="product-price">
                        $560
                    </span>
                    <a href="{{route('user.course.show',2)}}" class="product-name"> Supervised Learning: Error-correction Learning Algorithm</a>



                    <div class="small m-t-xs">
                        Many desktop publishing packages and web page editors now.
                    </div>
                    <div class="m-t text-righ">

                        <a href="{{route('user.class.show',1)}}" class="btn btn-xs btn-outline btn-primary"> เข้าเรียน <i
                                class="fa fa-long-arrow-right"></i> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </a>
</div>
@endsection

@section('script')

@endsection


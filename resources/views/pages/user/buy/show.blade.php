@extends('layouts.user.main')

@section('style')
<link href="/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/inspinia/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="/inspinia/css/plugins/codemirror/codemirror.css" rel="stylesheet">

@endsection

@section('content')
    <div class="row">
                <div class="col-md-6">
                    <div class="payment-card">
                        <img src="{{asset('scb.jpg')}}" width="20%">
                        
                        
                        <h2>
                            **** **** **** 1060
                        </h2>
                        <div class="row">
                            <div class="col-sm-6">
                                <small>
                                    <strong>Expiry date:</strong> 10/16
                                </small>
                            </div>
                            <div class="col-sm-6 text-right">
                                <small>
                                    <strong>ชื่อ :</strong> ศกุนตลา โชติกวี
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="payment-card">
                        <img src="{{asset('prompay.jpg')}} " width="30%">                       
                    <div class="col-md-8">    
                        <img src="{{asset('prompay2.jpg')}} " width="50%"> 
                        <h2>
                            0880231039
                        </h2>
                        <div class="row">
                            <div class="col-sm-6">
                                <small>
                                    <strong>Expiry date:</strong> 10/16
                                </small>
                            </div>
                            <div class="col-sm-6 text-right">
                                <small>
                                    <strong>ชื่อ :</strong> ศกุนตลา โชติกวี
                                </small>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            อัพโหลดใบเสร็จจ่ายเงิน
                        </div>
                        <div class="ibox-content">

                            
                            <form method="get" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">อัพโหลดรูป</label>
                                <div class="col-sm-10">
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                        <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
                                        <input type="file" name="image_file"></span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">ยกเลิก</button>
                                        
                                        <a class="btn btn-primary" href="{{route('user.bill.show',2)}}">ยืนยัน</a>
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('script')
<script src="/inspinia/js/plugins/validate/jquery.validate.min.js"></script>
<script src="/inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="/inspiniajs/plugins/codemirror/codemirror.js"></script>
<script src="/inspiniajs/plugins/codemirror/mode/xml/xml.js"></script>
<script>

</script>
@endsection
@extends('layouts.user.main')

@section('style')
    <link href="/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/inspinia/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-md-9">

        <div class="ibox">
            <div class="ibox-title">
                <span class="pull-right">(<strong>5</strong>) items</span>
                <h5>Items in your cart</h5>
            </div>
            <div class="ibox-content">


                <div class="table-responsive">
                    <table class="table shoping-cart-table">

                        <tbody>
                            <tr>
                                <td width="90">
                                    <div class="cart-product-imitation">
                                    </div>
                                </td>
                                <td class="desc">
                                    <h3>
                                        <a href="#" class="text-navy">
                                            Desktop publishing software
                                        </a>
                                    </h3>
                                    <p class="small">
                                        It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is
                                    </p>
                                    <dl class="small m-b-none">
                                        <dt>Description lists</dt>
                                        <dd>A description list is perfect for defining terms.</dd>
                                    </dl>

                                    <div class="m-t-sm">
                                        <a href="#" class="text-muted"><i class="fa fa-trash"></i> Remove item</a>
                                    </div>
                                </td>
                                <td width="65">
                                    <input type="text" class="form-control" placeholder="1">
                                </td>
                                <td>
                                    <h4>
                                        $180,00
                                    </h4>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
                
                    <div class="i-checks "><label> <input type="checkbox" value=""> <i></i></label></div>
                
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table shoping-cart-table">

                        <tbody>
                            <tr>
                                <td width="90">
                                    <div class="cart-product-imitation">
                                    </div>
                                </td>
                                <td class="desc">
                                    <h3>
                                        <a href="#" class="text-navy">
                                            Text editor
                                        </a>
                                    </h3>
                                    <p class="small">
                                        There are many variations of passages of Lorem Ipsum available
                                    </p>
                                    <dl class="small m-b-none">
                                        <dt>Description lists</dt>
                                        <dd>List is perfect for defining terms.</dd>
                                    </dl>

                                    <div class="m-t-sm">
                                        <a href="#" class="text-muted"><i class="fa fa-trash"></i> Remove item</a>
                                    </div>
                                </td>
                                <td width="65">
                                    <input type="text" class="form-control" placeholder="2">
                                </td>
                                <td>
                                    <h4>
                                        $100,00
                                    </h4>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="i-checks"><label> <input type="checkbox" value=""> <i></i></label></div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table shoping-cart-table">

                        <tbody>
                            
                            <tr>
                                <td width="90">
                                    <div class="cart-product-imitation">
                                    </div>
                                </td>
                                <td class="desc">
                                    <h3>
                                        <a href="#" class="text-navy">
                                            CRM software
                                        </a>
                                    </h3>
                                    <p class="small">
                                        Distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is
                                    </p>
                                    <dl class="small m-b-none">
                                        <dt>Description lists</dt>
                                        <dd>A description list is perfect for defining terms.</dd>
                                    </dl>

                                    <div class="m-t-sm">
                                        <a href="#" class="text-muted"><i class="fa fa-trash"></i> Remove item</a>
                                    </div>
                                </td>
                                <td width="65">
                                    <input type="text" class="form-control" placeholder="1">
                                </td>
                                <td>
                                    <h4>
                                        $110,00
                                    </h4>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="i-checks"><label> <input type="checkbox" value=""> <i></i></label></div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table shoping-cart-table">

                        <tbody>
                            
                            <tr>
                                <td width="90">
                                    <div class="cart-product-imitation">
                                    </div>
                                </td>
                                <td class="desc">
                                    <h3>
                                        <a href="#" class="text-navy">
                                            PM software
                                        </a>
                                    </h3>
                                    <p class="small">
                                        Readable content of a page when looking at its layout. The point of using Lorem
                                        Ipsum is
                                    </p>
                                    <dl class="small m-b-none">
                                        <dt>Description lists</dt>
                                        <dd>A description list is perfect for defining terms.</dd>
                                    </dl>

                                    <div class="m-t-sm">
                                        <a href="#" class="text-muted"><i class="fa fa-trash"></i> Remove item</a>
                                    </div>
                                </td>
                                <td width="65">
                                    <input type="text" class="form-control" placeholder="1">
                                </td>
                                <td>
                                    <h4>
                                        $130,00
                                    </h4>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="i-checks"><label> <input type="checkbox" value=""> <i></i></label></div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table shoping-cart-table">

                        <tbody>
                            
                            <tr>
                                <td width="90">
                                    <div class="cart-product-imitation">
                                    </div>
                                </td>
                                <td class="desc">
                                    <h3>
                                        <a href="#" class="text-navy">
                                            Photo editor
                                        </a>
                                    </h3>
                                    <p class="small">
                                        Page when looking at its layout. The point of using Lorem Ipsum is
                                    </p>
                                    <dl class="small m-b-none">
                                        <dt>Description lists</dt>
                                        <dd>A description list is perfect for defining terms.</dd>
                                    </dl>

                                    <div class="m-t-sm">
                                        <a href="#" class="text-muted"><i class="fa fa-trash"></i> Remove item</a>
                                    </div>
                                </td>
                                <td width="65">
                                    <input type="text" class="form-control" placeholder="1">
                                </td>
                                <td>
                                    <h4>
                                        $70,00
                                    </h4>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="i-checks"><label> <input type="checkbox" value=""> <i></i></label></div>
            </div>
         
            <div class="ibox-content">
                
                    <div class="i-checks"><label> <input type="radio" value="option1" name="a"> <i></i> เลือกทั้งหมด </label></div>
                    <div class="i-checks"><label> <input type="radio" checked="" value="option2" name="a"> <i></i> ลบทั้งหมด </label></div>
                
            <div class="text-center">
                 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    ยืนยัน
                </button>
            </div>
            </div>
        
        </div>

    </div>
    <div class="col-md-3">

        <div class="ibox">
            <div class="ibox-title">
                <h5>Cart Summary</h5>
            </div>
            <div class="ibox-content">
                <span>
                    Total
                </span>
                <h2 class="font-bold">
                    $390,00
                </h2>

                <hr />
                <div class="m-t-sm">
                    <div class="btn-group">
                        <a href="{{route('user.buy.show',2)}}" class="btn btn-primary btn-sm"><i class="fa fa-shopping-cart"></i> จ่ายเงิน</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="/inspinia/js/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });
            });
    </script>
@endsection
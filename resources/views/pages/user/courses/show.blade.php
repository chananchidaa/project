@extends('layouts.user.main')

@section('style')
<link href="/inspinia/css/plugins/slick/slick.css" rel="stylesheet">
<link href="/inspinia/css/plugins/slick/slick-theme.css" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">

        <div class="ibox product-detail">
            <div class="ibox-content">

                <div class="row">
                    <div class="col-md-5">
                        <div class="product-images">
                            <div>
                                <div class="image-imitation">
                                    <img src="{{$course->getUrlPath()}}" alt="" width="100%" height="100%">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-7">

                        <h2 class="font-bold m-b-xs">
                            {{$course->name}}
                        </h2>
                        <div class="m-t-md">
                            <h2 class="product-main-price"> ${{$course->price}} </h2>
                        </div>
                        <hr>

                        <h4>รายละเอียดคอร์สเรียน</h4>

                        <div class="small text-muted">
                            {{$course->detail}}
                        </div>
                        <hr>

                        <div>
                            <div class="btn-group">
                                <button class="btn btn-primary btn-sm"><i class="fa fa-cart-plus"></i>
                                    หยิบใส่ตะกร้า
                                </button>
                                <a href="{{route('user.buy.show',3)}}">
                                    <button class="btn btn-white btn-sm"><i class="fa fa-star"></i>
                                        ซื้อคอร์สเรียน
                                    </button>
                                </a>
                            </div>
                        </div>



                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
@endsection

@section('script')
<script src="/inspinia/js/plugins/slick/slick.min.js"></script>
<script>
    $(document).ready(function(){


        $('.product-images').slick({
            dots: true
        });

    });

</script>

@endsection
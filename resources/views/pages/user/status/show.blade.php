@extends('layouts.user.main')

@section('style')

@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight ecommerce">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                        <thead>
                            <tr>

                                <th data-toggle="true">Product Name</th>
                                <th data-hide="phone">Model</th>
                                <th data-hide="all">Description</th>
                                <th data-hide="phone">Price</th>
                                <th data-hide="phone,tablet">Quantity</th>
                                <th data-hide="phone">Status</th>
                                <th class="text-right" data-sort-ignore="true">Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    บทนำโครงข่ายประสาทเทียม
                                </td>
                                <td>
                                    Model 1
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $300.00
                                </td>
                                <td>
                                    1
                                </td>
                                <td>
                                    <span class="label label-primary">จ่ายแล้ว</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    พื้นฐานโครงข่ายประสาทเทียม
                                </td>
                                <td>
                                    Model 2
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $350.00
                                </td>
                                <td>
                                    1
                                </td>
                                <td>
                                    <span class="label label-primary">จ่ายแล้ว</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Perceptron Learning
                                </td>
                                <td>
                                    Model 3
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $400.00
                                </td>
                                <td>
                                    1
                                </td>
                                <td>
                                    <span class="label label-danger">ยังไม่จ่าย</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Supervised Learning: Error-correction Learning Algorithm
                                </td>
                                <td>
                                    Model 4
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $560.00
                                </td>
                                <td>
                                    2
                                </td>
                                <td>
                                    <span class="label label-primary">จ่ายแล้ว</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    บทนำโครงข่ายประสาทเทียม
                                </td>
                                <td>
                                    Model 5
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $300.00
                                </td>
                                <td>
                                    1
                                </td>
                                <td>
                                    <span class="label label-warning">รอการยืนยัน</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    พื้นฐานโครงข่ายประสาทเทียม
                                </td>
                                <td>
                                    Model 6
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $350.00
                                </td>
                                <td>
                                    2
                                </td>
                                <td>
                                    <span class="label label-danger">ยังไม่จ่าย</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Perceptron Learning
                                </td>
                                <td>
                                    Model 7
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $400.00
                                </td>
                                <td>
                                    1
                                </td>
                                <td>
                                    <span class="label label-danger">ยังไม่จ่าย</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Supervised Learning: Error-correction Learning Algorithm
                                </td>
                                <td>
                                    Model 8
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $560.00
                                </td>
                                <td>
                                    1
                                </td>
                                <td>
                                    <span class="label label-primary">จ่ายแล้ว</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Perceptron Learning
                                </td>
                                <td>
                                    Model 9
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $350.00
                                </td>
                                <td>
                                    1
                                </td>
                                <td>
                                    <span class="label label-primary">จ่ายแล้ว</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Supervised Learning: Error-correction Learning Algorithm
                                </td>
                                <td>
                                    Model 10
                                </td>
                                <td>
                                    ............................
                                </td>
                                <td>
                                    $560.00
                                </td>
                                <td>
                                    1
                                </td>
                                <td>
                                    <span class="label label-primary">จ่ายแล้ว</span>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group">
                                        <button class="btn-white btn btn-xs">View</button>
                                        <button class="btn-white btn btn-xs">Delete</button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="6">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('script')

@endsection
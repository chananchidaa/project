@extends('layouts.admin.main')

@section('list')
<div class="col-sm-12">
    <h2>คอร์สเรียนทั้งหมด</h2>
    <ol class="breadcrumb">
        <li>
            <a>จัดการ</a>
        </li>
        <li>
            <a href="{{route('admin.manages.course.index')}}">คอร์สเรียนทั้งหมด</a>
        </li>

        <li class="active">
            <strong>{{$course->name}}</strong>
        </li>

    </ol>
</div>



@endsection

@section('style')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>รายการบทเรียนทั้งหมด</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-12">
                        <span class="pull-right">
                            <a href="{{route('admin.manages.course.detail.create',$course->id)}}" class="btn btn-sm btn-primary"> สร้างบทเรียน</a>
                        </span>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ชื่อบทเรียน</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($course_details as $key =>$course_detail)
                                <tr>
                                    <th>{{$key+1}}</th>
                                    <td>{{$course_detail->name}}</td>
                                    <td>
                                        <a href="{{route('admin.manages.course.detail.edit',[$course->id,$course_detail->id])}}" class="btn btn-sm btn-primary">
                                            แก้ไขรายละเอียด</a>
                                        <form method="post" action="{{route('admin.manages.course.detail.destroy',[$course->id,$course_detail->id])}}"> 
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger">ลบบทเรียน</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection

@section('script')

@endsection
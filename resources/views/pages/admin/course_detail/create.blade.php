@extends('layouts.admin.main')

@section('style')
<link href="/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/inspinia/css/plugins/summernote/summernote.css" rel="stylesheet">
<link href="/inspinia/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
@endsection

@section('list')
<div class="col-sm-12">
    <h2>สร้างคอร์สเรียน</h2>
    <ol class="breadcrumb">
        <li>
            <a>จัดการ</a>
        </li>
        <li>
            <a href="{{route('admin.manages.course.index')}}">คอร์สเรียนทั้งหมด</a>
        </li>

        <li>
            <a href="{{route('admin.manages.course.detail.index',$course->id)}}">{{$course->name}}</a>
        </li>

        <li class="active">
            <strong>สร้างบทเรียน</strong>
        </li>
    </ol>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>สร้างบทเรียน</h5>
            </div>
            <div class="ibox-content">
                <form method="POST" action="{{route('admin.manages.course.detail.store',$course->id)}}" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group"><label class="col-sm-2 control-label">ชื่อบทเรียน</label>
                        <div class="col-sm-10"><input type="text" name="name" class="form-control"></div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="reset">ยกเลิก</button>
                            <button class="btn btn-primary" type="submit">สร้างบทเรียน</button>
                        </div>
                    </div>        
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>เนื้อหาในบทเรียน</h5>
            </div>
            <div class="ibox-content no-padding">
                <textarea name="detail" class="summernote"></textarea>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="/inspinia/js/plugins/validate/jquery.validate.min.js"></script>
<script src="/inspinia/js/plugins/summernote/summernote.min.js"></script>
<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 300, //set editable area's height
            codemirror: { // codemirror options
                theme: 'monokai'
            }
        });
    });
</script>
@endsection
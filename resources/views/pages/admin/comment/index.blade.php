@extends('layouts.admin.main')

@section('list')
<div class="col-sm-12">
    <h2>แจ้งเตือนความคิดเห็น</h2>
    <ol class="breadcrumb">
        <li>
            <a>จัดการ</a>
        </li>
        <li class="active">
            <strong>ความคิดเห็น</strong>
        </li>
    </ol>
</div>
@endsection

@section('style')

@endsection

@section('content')
<div class="col-lg-12 animated fadeInRight">
    <div class="mail-box-header">
        <h2>
            กล่องความคิดเห็น (..)
        </h2>
        <div class="mail-tools tooltip-demo m-t-md">
            <div class="btn-group pull-right">
                <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

            </div>
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox"><i
                    class="fa fa-refresh"></i>Refresh</button>
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as read"><i
                    class="fa fa-eye"></i> </button>
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mark as important"><i
                    class="fa fa-exclamation"></i> </button>
            <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to trash"><i
                    class="fa fa-trash-o"></i> </button>

        </div>
    </div>
    <div class="mail-box">

        <table class="table table-hover table-mail">
            <tbody>
                <tr class="unread">
                    <td class="check-mail">
                        <input type="checkbox" class="i-checks">
                    </td>
                    <td class="mail-ontact"><a href={{route('user.study.show',2)}}>>Anna Smith</a></td>
                    <td class="mail-subject"><a href={{route('user.study.show',2)}}>"mail_detail.html">Lorem ipsum dolor noretek imit set.</a></td>
                    <td class=""><i class="fa fa-paperclip"></i></td>
                    <td class="text-right mail-date">6.10 AM</td>
                </tr>
                <tr class="unread">
                    <td class="check-mail">
                        <input type="checkbox" class="i-checks" checked>
                    </td>
                    <td class="mail-ontact"><a href="mail_detail.html">Jack Nowak</a></td>
                    <td class="mail-subject"><a href="mail_detail.html">Aldus PageMaker including versions of Lorem
                            Ipsum.</a></td>
                    <td class=""></td>
                    <td class="text-right mail-date">8.22 PM</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('script')

@endsection
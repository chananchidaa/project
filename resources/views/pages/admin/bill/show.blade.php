@extends('layouts.admin.main')

@section('list')
<div class="col-sm-12">
    <h2>รายชื่อผู้สั่งซื้อคอร์สเรียน....ทั้งหมด</h2>
    <ol class="breadcrumb">
        <li>
            <a>จัดการ</a>
        </li>
        <li>
            <a href={{route('admin.manages.bill.index')}}>อนุมัติการชำระเงิน</a>
        </li>
        <li class="active">
            <strong>ตรวจสอบการชำระเงินคอร์สเรียน....</strong>
        </li>
    </ol>
</div>
@endsection

@section('style')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>รายชื่อผู้สั่งซื้อคอร์สเรียน...ทั้งหมด</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-12">
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>รายชื่อ</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>...</td>
                                <td>...</td>
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#myModal4">
                                        ดูใบเสร็จ
                                    </button>
                                    <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"
                                        aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content animated fadeIn">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span
                                                            aria-hidden="true">&times;</span><span
                                                            class="sr-only">Close</span></button>
                                                    <i class="fa fa-clock-o modal-icon"></i>
                                                    <h4 class="modal-title">Modal title1</h4>
                                                    <small>Lorem Ipsum is simply dummy text of the printing and
                                                        typesetting industry.</small>
                                                </div>
                                                <div class="modal-body">
                                                    <p><strong>Lorem Ipsum is simply dummy</strong> text of the printing
                                                        and typesetting industry. Lorem Ipsum has been the industry's
                                                        standard dummy text ever since the 1500s, when an unknown
                                                        printer took a galley of type and scrambled it to make a type
                                                        specimen book. It has survived not only five centuries, but also
                                                        the leap into electronic typesetting,
                                                        remaining essentially unchanged.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary">อนุมัติการชำระเงิน</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection

@section('script')

@endsection
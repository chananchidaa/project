@extends('layouts.admin.main')

@section('list')
<div class="col-sm-12">
    <h2>รายการสั่งซื้อคอร์สเรียนทั้งหมด</h2>
    <ol class="breadcrumb">
        <li>
            <a>จัดการ</a>
        </li>
        <li class="active">
            <strong>อนุมัติการชำระเงิน</strong>
        </li>
    </ol>
</div>
@endsection

@section('style')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>รายการสั่งซื้อคอร์สเรียนทั้งหมด</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-12">
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ชื่อคอร์สเรียน</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>...</td>
                                <td>...</td>
                                <td><a href={{route('admin.manages.bill.show',2)}} class="btn btn-sm btn-primary">
                                        ตรวจสอบ</a>
                                </td>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection

@section('script')

@endsection
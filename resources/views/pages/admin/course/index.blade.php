@extends('layouts.admin.main')

@section('list')
<div class="col-sm-12">
    <h2>คอร์สเรียนทั้งหมด</h2>
    <ol class="breadcrumb">
        <li>
            <a>จัดการ</a>
        </li>
        <li class="active">
            <strong>คอร์สเรียน</strong>
        </li>
    </ol>
</div>



@endsection

@section('style')

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>รายการคอร์สทั้งหมด</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-12">
                        <span class="pull-right">
                            <a href={{route('admin.manages.course.create')}} class="btn btn-sm btn-primary">
                                สร้างคอร์สเรียน</a>
                        </span>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ชื่อคอร์สเรียน</th>
                                <th>จำนวนผู้เรียน</th>
                                <th>จำนวนบทเรียน</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($courses as $key =>$course)
                            <tr>
                                <th>{{$key+1}}</th>
                                <td>{{$course->name}}</td>
                                <td>...</td>
                                <td>...</td>
                                <td>
                                    <a href="{{route('admin.manages.course.detail.index',$course->id)}}"
                                        class="btn btn-sm btn-primary">
                                        เพิ่มเนื้อหาคอร์สเรียน</a>
                                    <a href="{{route('admin.manages.course.edit',$course->id)}}"
                                        class="btn btn-sm btn-primary">
                                        แก้ไขคอร์สเรียน</a>
                                    <form action="{{route('admin.manages.course.destroy', $course->id) }}" method="POST"><br>
                                        @csrf
                                        <input type="hidden" class="form-control" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-sm btn-danger"><i
                                                class="icon-trash2"></i>ลบคอร์สเรียน</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection

@section('script')

@endsection
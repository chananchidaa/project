@extends('layouts.admin.main')

@section('style')
<link href="/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/inspinia/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
<link href="/inspinia/css/plugins/codemirror/codemirror.css" rel="stylesheet">
@endsection

@section('list')
<div class="col-sm-12">
    <h2>สร้างคอร์สเรียน</h2>
    <ol class="breadcrumb">
        <li>
            <a>จัดการ</a>
        </li>
        <li>
            <a href="{{route('admin.manages.course.index')}}">คอร์สเรียนทั้งหมด</a>
        </li>
        <li class="active">
            <strong>สร้างคอร์สเรียน</strong>
        </li>
    </ol>
</div>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>สร้างคอร์สเรียน</h5>
            </div>
            <div class="ibox-content">
                <form method="POST" action="{{route('admin.manages.course.store')}}" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group"><label class="col-sm-2 control-label">ชื่อคอร์ส</label>
                        <div class="col-sm-10"><input type="text" name="name" class="form-control"></div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group"><label class="col-sm-2 control-label">รายละเอียดคอร์สเรียน</label>
                        <div class="col-sm-10"><textarea type="text" name="detail" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group"><label class="col-sm-2 control-label">ราคาคอร์สเรียน</label>
                        <div class="col-sm-10"><input type="number" name="price" class="form-control"></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">อัปโหลดรูปคอร์สเรียน</label>
                        <div class="col-sm-10">
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
                                <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span>
                                <input type="file" name="image_file"></span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-white" type="reset">ยกเลิก</button>
                            <button class="btn btn-primary" type="submit">สร้างคอร์สเรียน</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="/inspinia/js/plugins/validate/jquery.validate.min.js"></script>
<script src="/inspinia/js/plugins/jasny/jasny-bootstrap.min.js"></script>
<script src="/inspiniajs/plugins/codemirror/codemirror.js"></script>
<script src="/inspiniajs/plugins/codemirror/mode/xml/xml.js"></script>
<script>

</script>
@endsection
@extends('layouts.admin.main')

@section('list')
<div class="col-sm-12">
    <h2>เพิ่มเนื้อหาคอร์สเรียน</h2>
    <ol class="breadcrumb">
        <li>
            <a href="index.html">จัดการ</a>
        </li>
         <li>
            <a href="{{route('admin.manages.course.index')}}">คอร์สเรียนทั้งหมด</a>
        </li>
        <li class="active">
            <strong>เพิ่มเนื้อหาคอร์สเรียน</strong>
        </li>
    </ol>
</div>
@endsection

@section('style')
    
@endsection

@section('content')
    
@endsection

@section('script')

@endsection


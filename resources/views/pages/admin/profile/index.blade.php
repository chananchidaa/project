@extends('layouts.admin.main')

@section('list')
<div class="col-sm-12">
    <h2>โปรไฟล์</h2>
</div>
@endsection

@section('style')

@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg m-t-lg">
        <div class="col-md-8">

            <div class="profile-image">
                <img src="{{asset('inspinia/img/profile_small.jpg')}}" class="img-circle circle-border m-b-md" alt="profile">
            </div>
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">
                            {{$user->name}}
                        </h2>
                        <h4>Founder of Groupeq</h4>
                        <small>
                            There are many variations of passages of Lorem Ipsum available, but the majority
                            have suffered alteration in some form Ipsum available.
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>ข้อมูลโปรไฟล์</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="height:360px">
                <form method="post" action="{{route('admin.manages.profile.update',$user->id)}}" class="form-horizontal">
                    @csrf @method('PATCH')
                    <div class="form-group"><label class="col-sm-2 control-label">ชื่อ</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{$user->name}}"></div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">E-mail</label>
                        <div class="col-sm-10"><input type="email" class="form-control" name="email" value="{{$user->email}}"></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="username" value="{{$user->username}}"></div>
                    </div>



                    <div class="hr-line-dashed"></div>
                    <div class="form-group"><label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10"><input type="password" class="form-control" name="password"></div>
                    </div>
                    
                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-sm btn-primary">บันทึกโปรไฟล์</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
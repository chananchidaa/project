    <!-- Mainly scripts -->
    <script src="/inspinia/js/jquery-3.1.1.min.js"></script>
    <script src="/inspinia/js/bootstrap.min.js"></script>
    <script src="/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/inspinia/js/inspinia.js"></script>
    <script src="/inspinia/js/plugins/pace/pace.min.js"></script>
<nav class="navbar navbar-static-top" role="navigation">
    <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
            class="navbar-toggle collapsed" type="button">
            <i class="fa fa-reorder"></i>
        </button>
        <a href="{{route('user.dashboard.index')}}" class="navbar-brand">CourseAI</a>
    </div>

    <div class="navbar-collapse collapse" id="navbar">
        @if (Auth::check())
            <ul class="nav navbar-nav">
            <li class="active">
                <a href="{{route('user.mycourse.show',2)}}" aria-expanded="false" role="button" href="layouts.html"> คอร์สเรียนของฉัน </a>
            </li>
            <li class="active">
                <a href="{{route('user.status.show',2)}}" aria-expanded="false" role="button" href="layouts.html"> สถานะการชำระเงิน </a>
            </li>
        </ul>
        @endif
        
        <ul class="nav navbar-top-links navbar-right">
            @if (Auth::check())
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i> <span class="label label-primary">8</span>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                <span class="pull-right text-muted small">12 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="#">
                            <div>
                                <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                <span class="pull-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="notifications.html">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{route('user.cart.show',2)}}"><i class="fa fa-shopping-cart"></i>
                    <span class="nav-label"></span>
                </a>
            </li>
            {{-- <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> ออกจากระบบ
                </a>
            </li> --}}
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-user-circle" aria-hidden="true" ></i>
                </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{route('user.profile.show',2)}}">Profile</a></li>
                        
                        
                        <li class="divider"></li>
                        <li>
                            <a onclick="event.preventDefault();  document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> ออกจากระบบ
                            </a>
                        </li>
                    </ul>
            </li>
            
    @else
    <li>
        <a href="{{route('login')}}">
            <i class="fa fa-sign-out"></i> เข้าสู่ระบบ
        </a>
    </li>
    @endif

    </ul>
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</nav>


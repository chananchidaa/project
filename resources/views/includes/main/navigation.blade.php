
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" src="{{asset('/inspinia/img/profile_small.jpg')}}" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"></strong>
                            </span> <span class="text-muted text-xs block">{{Auth::user()->name}} <b class="caret"></b></span>
                        </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href={{route('admin.manages.profile.index')}}>โปรไฟล์</a></li>
                        
                        
                        <li class="divider"></li>
                        <li>
                            <a onclick="event.preventDefault();  document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> ออกจากระบบ
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>


            <li>
                <a href="index.html"><i class="fa fa-th-large"></i>จัดการ <span class="nav-label"></span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{route('admin.manages.course.index')}}">คอร์สเรียน</a></li>
                    <li><a href="{{route('admin.manages.bill.index')}}">อนุมัติการชำระเงิน</a></li>
                    <li><a href="{{route('admin.manages.student.index')}}">รายชื่อผู้เรียน</a></li>
                </ul>
            </li>
        </ul>
    </div>


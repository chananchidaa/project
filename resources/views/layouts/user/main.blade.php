<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>INSPINIA | Dashboard v.4</title>

    <link href="/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/inspinia/css/animate.css" rel="stylesheet">
    <link href="/inspinia/css/style.css" rel="stylesheet">
    @yield('style')
     @include('includes.user.script')
</head>

<body class="top-navigation">

    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
                @include('includes.user.nav')
            </div>
            <div class="wrapper wrapper-content">
                <div class="container">
                    @yield('content')
                </div>
            </div>
            <div class="footer">
                @include('includes.user.footer')
            </div>

        </div>
    </div>
   
    @yield('script')
</body>

</html>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HOME</title>
    @include('includes.main.style')
    @yield('style')
</head>

<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            @include('includes.main.navigation')
        </nav>

        <div id="page-wrapper" class="gray-bg">

            @include('includes.main.header')

            <div class="row wrapper border-bottom white-bg page-heading">
                @yield('list')
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                @include('includes.main.alert')
                @yield('content')
            </div>

            @include('includes.main.footer')
        </div>

    </div>
    {{-- SCRIPT --}}
    @include('includes.main.script')
    @yield('script')
</body>

</html>

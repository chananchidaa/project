<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // check permission
        if (Auth::user()->hasRole('admin')) {
            return redirect()->route('admin.dashboard.index');
        } elseif (Auth::user()->hasRole('user')) {
            return redirect()->route('user.dashboard.index');
        } else {
            // return view('permision');
        }
    }
}

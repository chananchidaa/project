<?php

namespace App\Http\Controllers\Admin\Manages;

use App\Course;
use App\CourseDetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($course_id)
    {
        $course = Course::find($course_id);
        $course_details = $course->course_details;
        return view('pages.admin.course_detail.index', compact('course', 'course_details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($course_id)
    {
        $course = Course::find($course_id);
        return view('pages.admin.course_detail.create', compact('course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $course_id)
    {
        DB::beginTransaction();
        $course_detail = new CourseDetail;
        $course_detail->course_id = $course_id;
        $course_detail->name = $request->name;
        $course_detail->detail = $request->detail;
        $course_detail->save();
        DB::commit();
        return redirect()->route('admin.manages.course.detail.index', $course_id)->with('message', 'สร้างบทเรียนสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages.admin.course_detail.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($course_id, $course_detail_id)
    {
        $course_detail = CourseDetail::find($course_detail_id);
        return view('pages.admin.course_detail.edit', compact('course_detail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $course_id, $course_detail_id)
    {
        DB::beginTransaction();
        $course_detail = CourseDetail::find($course_detail_id);
        $course_detail->name = $request->name;
        $course_detail->detail = $request->detail;
        $course_detail->save();
        DB::commit();
        return redirect()->route('admin.manages.course.detail.index', $course_id)->with('message', 'แก้ไขบทเรียนสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($course_id, $course_detail_id)
    {
        DB::beginTransaction();
        $course_detail = CourseDetail::find($course_detail_id);
        $course_detail->delete();
        DB::commit();
        return redirect()->route('admin.manages.course.detail.index', $course_id)->with('message', 'ลบบทเรียนสำเร็จ');
    }
}

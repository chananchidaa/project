<?php

namespace App\Http\Controllers\Admin\Manages;

use App\Course;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadController as UploadController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();
        return view('pages.admin.course.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        if ($request->hasFile('image_file')) {
            $image_path = UploadController::upload_file($request->file('image_file'));
            $course = new Course;
            $course->name = $request->name;
            $course->detail = $request->detail;
            $course->price = $request->price;
            $course->image = $image_path;
            $course->save();
        }
        DB::commit();
        return redirect()->route('admin.manages.course.index')->with('message', 'สร้างคอร์สเรียนสำเร็จ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages.admin.course.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);
        return view('pages.admin.course.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        $course = Course::find($id);
        $course->name = $request->name;
        $course->detail = $request->detail;
        $course->price = $request->price;
        if ($request->hasFile('image_file')) {
            $image_path = UploadController::upload_file($request->file('image_file'));
            $course->image = $image_path;
        }
        $course->save();

        DB::commit();
        return redirect()->route('admin.manages.course.index')->with('message', 'แก้ไขคอร์สเรียนสำเร็จ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($course_id)
    {
        $course = Course::find($course_id);
        $course->delete();

        return redirect()->route('admin.manages.course.index')->with('message', 'ลบคอร์สเรียนสำเร็จ');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Image;

class UploadController extends Controller
{

    public static function upload_file($image_file)
    {
        $fileName = time() . '.' . $image_file->getClientOriginalExtension();
        $img = Image::make($image_file->getRealPath());
        $img->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->stream();
        $path = 'images/course/profile/' . $fileName;
        Storage::disk('public')->put($path, $img, 'public');
        return $path;
    }

}

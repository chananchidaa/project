<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function course_details()
    {
        return $this->hasMany(CourseDetail::class);
    }

    public function getUrlPath()
    {
        return asset('storage/' . $this->image);
    }
}

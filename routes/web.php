<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Auth::routes();
Route::resource('/', 'DashboardController');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('home', 'HomeController');

    //admin
    Route::group(['prefix' => 'admin', 'middleware' => ['role:admin'], 'as' => 'admin.'], function () {
        Route::post('logout', 'Admin\DashboardController@logout')->name('logout');
        Route::resource('dashboard', 'Admin\DashboardController');

        Route::name('manages.')->group(function () {
            Route::resource('manages/course', 'Admin\Manages\CourseController');
            Route::resource('manages/course.detail', 'Admin\Manages\CourseDetailController');
            Route::resource('manages/bill', 'Admin\Manages\BillController');
            Route::resource('manages/profile', 'Admin\Manages\ProfileController');
            Route::resource('manages/student', 'Admin\Manages\StudentController');
            Route::resource('manages/comment', 'Admin\Manages\CommentController');

        });
    });

    //user
    Route::group(['prefix' => 'user', 'middleware' => ['role:user|admin'], 'as' => 'user.'], function () {
        Route::resource('dashboard', 'User\DashboardController');
        Route::resource('course', 'User\CourseController');
        Route::resource('buy', 'User\BuyController');
        Route::resource('bill', 'User\BillController');
        Route::resource('status', 'User\StatusController');
        Route::resource('mycourse', 'User\MycourseController');
        Route::resource('class', 'User\ClassController');
        Route::resource('cart', 'User\CartController');
        Route::resource('study', 'User\StudyController');
        Route::resource('profile', 'User\ProfileController');
    });
});

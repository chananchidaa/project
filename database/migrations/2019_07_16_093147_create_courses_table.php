<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->comment('ชื่อคอร์สเรียน');
            $table->string('detail', 50)->comment('รายละเอียดคอร์ดเรียน');
            $table->decimal('price', 30, 2)->comment('ราคา');
            $table->string('image')->comment('ที่อยู่รูปภาพ path , link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}

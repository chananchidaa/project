<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('ชื่อบิล');
            $table->string('user_id')->comment('บิลของใคร');
            $table->string('image')->comment('รูปใบเสร็จ');
            $table->tinyInteger('status')->comment('แอดมินเห็นว่าชำระเงินรึยัง');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}

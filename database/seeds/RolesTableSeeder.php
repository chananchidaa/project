<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            ['display_name' => 'ผู้ดูแลระบบ', 'name' => 'admin', 'description' => 'ผู้ดูแลระบบ'],
            ['display_name' => 'ผู้ใช้งาน', 'name' => 'user', 'description' => 'ผู้ใช้งาน'],
        ];
        foreach ($datas as $key => $data) {
            Role::firstOrCreate([
                'name' => $data['name'],
            ], $data);
        }
    }
}

<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            ['name' => 'full-control', 'display_name' => 'full-control', 'description' => 'ทำได้ปกติ ตามประเภทผู้ใช้'],

            ['name' => 'write-lotto', 'display_name' => 'write-lotto', 'description' => 'ดูของรวม / คาดคะเนได้เสีย, ดูของแยกตามประเภท, ดูของแยกตามสมาชิก, รายการที่ถูกยกเลิก, ตั้งค่าการรับของ'],
            ['name' => 'write-user', 'display_name' => 'write-user', 'description' => 'เพิ่มสมาชิก, รายชื่อสมาชิก, แก้ไขสมาชิก, สมาชิกออนไลน์'],
            ['name' => 'write-take-list', 'display_name' => 'write-take-list', 'description' => 'รายการเก็บของ / อั้น'],
            ['name' => 'write-reports', 'display_name' => 'write-reports', 'description' => 'ได้เสียตามสมาชิก, ได้เสียตามประเภท, รายการที่ถูกรางวัล, ผลการออกรางวัล'],
            ['name' => 'write-transfers', 'display_name' => 'write-transfers', 'description' => 'โอนเงิน'],
            ['name' => 'write-shop', 'display_name' => 'write-shop', 'description' => 'จัดการหน้าร้าน'],
            ['name' => 'write-cancel', 'display_name' => 'write-cancel', 'description' => 'ยกเลิกการแทง'],

            ['name' => 'read-lotto', 'display_name' => 'read-lotto', 'description' => 'ดูของรวม / คาดคะเนได้เสีย, ดูของแยกตามประเภท, ดูของแยกตามสมาชิก, รายการที่ถูกยกเลิก, ตั้งค่าการรับของ'],
            ['name' => 'read-user', 'display_name' => 'read-user', 'description' => 'เพิ่มสมาชิก, รายชื่อสมาชิก, แก้ไขสมาชิก, สมาชิกออนไลน์'],
            ['name' => 'read-take-list', 'display_name' => 'read-take-list', 'description' => 'รายการเก็บของ / อั้น'],
            ['name' => 'read-reports', 'display_name' => 'read-reports', 'description' => 'ได้เสียตามสมาชิก, ได้เสียตามประเภท, รายการที่ถูกรางวัล, ผลการออกรางวัล'],
            ['name' => 'read-transfers', 'display_name' => 'read-transfers', 'description' => 'โอนเงิน'],
            ['name' => 'read-shop', 'display_name' => 'read-shop', 'description' => 'จัดการหน้าร้าน'],
            ['name' => 'read-cancel', 'display_name' => 'read-cancel', 'description' => 'ยกเลิกการแทง'],
        ];
        foreach ($datas as $key => $data) {
            $permission = Permission::firstOrCreate([
                'name' => $data['name'],
            ], $data);
        }
    }
}

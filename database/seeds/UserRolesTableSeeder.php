<?php

use App\User;
use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::all();
        $user[0]->roles()->attach(1);
        $user[1]->roles()->attach(2);

    }
}

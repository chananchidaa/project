<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = [
            ['name' => 'ผู้ดูแลระบบ', 'username' => 'admin', 'email' => 'admin@admin.com', 'password' => Hash::make('123456')],
            ['name' => 'ผู้ใช้งาน', 'username' => 'user', 'email' => 'user@user.com', 'password' => Hash::make('123456')],
        ];
        foreach ($datas as $key => $data) {
            User::firstOrCreate(['name' => $data['name']], $data);
        }
    }
}
